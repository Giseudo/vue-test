var path = require('path');
var webpack = require('webpack');
var merge = require('webpack-merge');
var baseWebpackConfig = require('./webpack.base');
var CompressionPlugin = require('compression-webpack-plugin');

module.exports = merge(baseWebpackConfig, {
	devtool: 'cheap-source-map',
	entry: {
		'js/aev': './dist/main.js',
	},
	output: {
		path: path.join(__dirname, '../public'),
		publicPath: '/',
		filename: '[name].bundle.js',
	},
	plugins: [
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false
		}),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$/,
			threshold: 10240,
			minRatio: 0.8
		})
	]
})
