var express = require('express'),
	request = require('request'),
	path = require('path');

let app = express(),
	port = process.env.PORT || 8080;

if (process.env.NODE_ENV == 'production') {
	// Apply gzip compression to js files
	app.get('*.js', function(req, res, next) {
		req.url = req.url + '.gz';
		res.setHeader('Content-Encoding', 'gzip');
		res.setHeader('Content-Type', 'text/javascript');
		next();
	});
}

// Routes
app.use('/', express.static(path.join(__dirname, 'public')));

// Error middlware
app.use((err, req, res, next) => {
	res.status(err.status || 500);
	res.send(err.message);

	console.log(err.stack);
});

// Listen to default port
app.listen(port, (err) => {
	if (err)
		console.error(`Error: ${err}`);

	console.log(`Server started ${port}`);
});

module.exports = app;
