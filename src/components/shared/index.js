export default function install(Vue) {
	Vue.component('gi-tabs', require('./Tabs'));
	Vue.component('gi-tab', require('./Tab'));
	Vue.component('gi-textarea', require('./Textarea'));
	Vue.component('gi-button', require('./Button'));
	Vue.component('gi-select', require('./Select'));
	Vue.component('gi-text', require('./Text'));
}
