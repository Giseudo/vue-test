import Vue from 'vue';
import Components from './components/shared';

// Init shared components
Vue.use(Components);

export default new Vue({
	el: '#app',
	render: createElement => createElement(require('./App'))
});
